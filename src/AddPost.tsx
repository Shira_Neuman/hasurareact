/* eslint-disable @typescript-eslint/no-use-before-define */
import React, { useEffect, useState } from 'react';
import './App.css';
import { INSERT_POST, GET_MY_POSTS } from './Queries';
import { useMutation } from '@apollo/react-hooks'
import { makeStyles, TextField, Button } from '@material-ui/core'
import { Post } from './Models/Post';
import { pick } from 'lodash';

const useStyles = makeStyles({
    form: {
        display: 'flex',
        flexDirection: 'column'
    },
    textField: {
        padding: '5px',
        width: 200,
        marginTop: '5px'
    },
});

const initPost = (): Post => {
    return {
        body: "",
        createdAt: new Date(Date.now()),
        headline: "",
        id: 0
    };
}

const AddPost = () => {
    const classes = useStyles();
    const [post, setPost] = useState(initPost())
    const resetInput = () => {
        setPost(initPost());
    };

    const handleChangePost = (event: any) => {
        console.log({ event })
        const eventId = event.target.id;
        const eventValue = event.target.value;
        setPost(oldPost => ({ ...oldPost, [eventId]: eventValue }))
    }

    const updateCache = (cache: any, { data }: any) => {
        // Fetch the todos from the cache
        const existingPosts = cache.readQuery({
            query: GET_MY_POSTS
        });
        // Add the new todo to the cache
        const newPost = data.insert_shira_schema_post.returning[0];
        cache.writeQuery({
            query: GET_MY_POSTS,
            data: { shira_schema_post: [newPost, ...existingPosts.shira_schema_post] }
        });
    };

    const [addPost] = useMutation(INSERT_POST, { onCompleted: resetInput, update: updateCache });

    const handleClickAdd = async () => {
        try {
            const response = await addPost({ variables: pick(post, ['authorId', 'body', 'headline', 'createdAt']) });
            console.log({ response })
        } catch (error) {
            console.log(error)
        }
    }

    return (
        <form>
            <TextField
                id="body"
                label="Body"
                onChange={handleChangePost}
                defaultValue={post.body}
                className={classes.textField}
            />
            <TextField
                id="headline"
                label="Headline"
                onChange={handleChangePost}
                defaultValue={post.headline}
                className={classes.textField}
            />
            <TextField
                id="createdAt"
                label="Creation Date"
                type="date"
                defaultValue={post.createdAt.toString()}
                className={classes.textField}
                InputLabelProps={{
                    shrink: true,
                }}
                onChange={handleChangePost}
            />
            <Button onClick={handleClickAdd}>Add Post</Button>
        </form>
    );
}

export default AddPost;
