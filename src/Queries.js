import gql from 'graphql-tag';

export const GET_MY_PERSONS = gql`
query MyPersons {
    shira_schema_person {
      about
      created_at
      first_name
      id
      last_name
      posts {
        author_id
        body
        created_at
        headline
        id
      }
    }
  }`;

export const GET_MY_POSTS = gql`
query MyQuery {
    shira_schema_post {
      author_id
      created_at
      body
      headline
      id
      person {
        last_name
        id
        first_name
        about
        created_at
      }
    }
  }`

  export const INSERT_PERSON = gql`
  mutation ($firstName: string, $lastName: string, $about: string) {
    insert_shira_schema_person(objects: {about: $about, first_name: $firstName, last_name: $lastName}) {
      affected_rows
      returning {
        last_name
        id
        first_name
        created_at
        about
      }
    }
  }`

  export const INSERT_POST = gql`
  mutation($authorId: Int
    $body: String
    $createdAt: timestamp
    $headline: String) {
    insert_shira_schema_post(objects: {author_id: $authorId, body: $body, created_at: $createdAt, headline: $headline}) {
      affected_rows
      returning {
        author_id
        created_at
        body
        headline
        id
        person {
          last_name
          id
          first_name
          about
          created_at
        }
      }
    }
  }`


  export const INSERT_POST_NO_AUTHOR =  gql`
  mutation($body: String
    $createdAt: timestamp
    $headline: String) {
    insert_shira_schema_post(objects: {body: $body, created_at: $createdAt, headline: $headline}) {
      affected_rows
      returning {
        author_id
        body
        created_at
        headline
        id
      }
    }
  }`

  export const DELETE_POST = gql`
  mutation($id: Int) {
    delete_shira_schema_post(where: {id: {_eq: $id}}) {
      affected_rows
      returning {
        author_id
        body
        created_at
        headline
        id
        person {
          about
          created_at
          first_name
          id
          last_name
        }
      }
    }
  }`
