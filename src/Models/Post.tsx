export interface Post {
    authorId?: number;
    createdAt: Date;
    body: string;
    headline: string;
    id: number;
    person?: Person;
}

export interface Person {
    lastName: string;
    id: number;
    firstName: string;
    createdAt: Date;
    about: string;
}