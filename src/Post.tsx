import React, { useEffect, useState } from 'react';
import './App.css';
import { Button, Paper, Card, makeStyles, CardContent, Typography, CardActions } from '@material-ui/core'
import { Post } from './Models/Post';
// import {Delete} from '@material-ui/core/'

interface PostProps {
    post: Post;
}

const useStyles = makeStyles({
    root: {
        minWidth: 275,
        margin: '20px'
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
});

const PostCard = (props: PostProps) => {
    const classes = useStyles();
    const { post } = props;

    return (
        <Card className={classes.root} variant="outlined">
            <CardContent>
                <Typography className={classes.title} color="textSecondary" gutterBottom>
                    {post.person ? `${post.person?.firstName} ${post.person?.lastName}` : ''}
                </Typography>
                <Typography variant="h5" component="h2">
                    {post.headline}
                </Typography>
                <Typography className={classes.pos} color="textSecondary">
                    {post.createdAt.toString()}
                </Typography>
                <Typography variant="body2" component="p">
                    {post.body}
                </Typography>
            </CardContent>
            <CardActions>
                
            </CardActions>
        </Card>
    );
}

export default PostCard;
