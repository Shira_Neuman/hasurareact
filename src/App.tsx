import React, { useEffect , useState} from 'react';
import './App.css';
import ApolloClient from 'apollo-client';
 import { InMemoryCache } from 'apollo-cache-inmemory';
 import { HttpLink } from 'apollo-link-http';
 import { ApolloProvider } from '@apollo/react-hooks';
 import CircularProgress from '@material-ui/core/CircularProgress';
import PostList from './PostsList';

const getToken = async() => {
  return await fetch('http://localhost:5000/login', 
  {method: 'POST'})
}

 const createApolloClient = (authToken: string) => {
  return new ApolloClient({
    link: new HttpLink({
      uri: 'http://localhost:8080/v1/graphql',
      headers: {
        Authorization: `Bearer ${authToken}`,
        'X-Hasura-Role': 'editor'
      }
    }),
    cache: new InMemoryCache(),
  });
 };

const getCookie = () => {
  const cookie = document.cookie;
  const cookieArray = cookie.split(';');
  const cookieToken = cookieArray.find(cookie => cookie.includes("token"))
  return cookieToken ? cookieToken.substring(7) : null;
  // console.log(cookieToken.substring(7))
  // console.log({cookieArray});
}

const App = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [client, setClient] = useState(createApolloClient(""))
  getCookie();

  useEffect(() => {
    const token = getCookie();
    if (token) {
      setClient(createApolloClient(token))
      setIsLoading(false)
    } else {
      getToken().then(response => {
        if (!response.ok) {
          console.log("response")
        }
        return response.json()
      }).then(json => {
        console.log({json});
        setClient(createApolloClient(json))
        setIsLoading(false);
        document.cookie = "token=" + json.token;
      }).catch(error => console.log(error))
    }
  }, [])
  
  return (
      isLoading ? <CircularProgress size={150}/> : (<ApolloProvider client={client}>
      <PostList/>
   </ApolloProvider>)
  );
}

export default App;
