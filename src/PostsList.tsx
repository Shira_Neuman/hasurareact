import React, { useEffect, useState, Fragment } from 'react';
import './App.css';
import CircularProgress from '@material-ui/core/CircularProgress';
import { GET_MY_POSTS, INSERT_PERSON } from './Queries';
import { useQuery, useMutation } from '@apollo/react-hooks'
import { Button, Grid, Card, CardContent } from '@material-ui/core'
import { Post, Person } from './Models/Post';
import PostCard from './Post';
import AddPost from './AddPost';
import {camelCase, mapKeys} from 'lodash';

const PostList = () => {
    const { loading, error, data } = useQuery(GET_MY_POSTS);
    return (
        loading ? <CircularProgress size={150} /> : error ? <div>{`error: ${error}`}</div> :
            <Fragment>
                <Grid container>
                    <Grid item xs={5}>
                        <AddPost key={data.shira_schema_post} />
                    </Grid>
                    <Grid item xs={7}>
                        <Card>
                            <CardContent>
                                {(data.shira_schema_post as Post[]).map((post) => {
                                    const newPost =(mapKeys(post, (value,key) => camelCase(key)) as any) as Post;
                                    newPost.person = newPost.person ? (mapKeys(newPost.person, (value,key) => camelCase(key)) as any) as Person : newPost.person
                                    console.log({newPost})
                                    return <PostCard key={post.id} post={newPost} />
                                })}
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
            </Fragment>
    );
}

export default PostList;
